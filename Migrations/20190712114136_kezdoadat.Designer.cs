﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using fenykepalbum_5_3.AspNetCore.NewDb.Models;

namespace fenykepalbum_5_3.Migrations
{
    [DbContext(typeof(fenykepContext))]
    [Migration("20190712114136_kezdoadat")]
    partial class kezdoadat
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("fenykepalbum_5_3.AspNetCore.NewDb.Models.Kepek", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Filenev");

                    b.Property<string>("Holkeszult");

                    b.Property<string>("Leiras");

                    b.Property<DateTime>("Mikor");

                    b.HasKey("Id");

                    b.ToTable("Kepek");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Filenev = "WP_20150531_15_50_32_Pro.jpg",
                            Holkeszult = "Lednice",
                            Leiras = "Kastély park, tó",
                            Mikor = new DateTime(2015, 5, 31, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 2,
                            Filenev = "WP_20150531_16_01_58_Pro.jpg",
                            Holkeszult = "Lednice",
                            Leiras = "Kastély park, tó",
                            Mikor = new DateTime(2015, 5, 31, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 3,
                            Filenev = "WP_20161124_14_22_05_Pro.jpg",
                            Holkeszult = "ÓTátrafüred",
                            Leiras = "Felhőbe burkolózó hegycsúcs",
                            Mikor = new DateTime(2016, 11, 24, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 4,
                            Filenev = "WP_20171025_14_09_38_Pro.jpg",
                            Holkeszult = "Tátralomnic",
                            Leiras = "Hegycsúcs",
                            Mikor = new DateTime(2017, 10, 25, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 5,
                            Filenev = "WP_20171025_14_10_37_Pro.jpg",
                            Holkeszult = "Tátralomnic",
                            Leiras = "Kilátás a hegyről",
                            Mikor = new DateTime(2017, 10, 25, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 6,
                            Filenev = "WP_20171025_14_23_14_Pro.jpg",
                            Holkeszult = "Tátralomnic",
                            Leiras = "Lanovka",
                            Mikor = new DateTime(2017, 10, 25, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 7,
                            Filenev = "WP_20171025_14_35_07_Pro.jpg",
                            Holkeszult = "Tátralomnic",
                            Leiras = "Hegyi tó",
                            Mikor = new DateTime(2017, 10, 25, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 8,
                            Filenev = "WP_20180806_10_34_31_Pro.jpg",
                            Holkeszult = "Capri",
                            Leiras = "Kilátás a hegyről",
                            Mikor = new DateTime(2018, 8, 6, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 9,
                            Filenev = "WP_20180806_11_10_49_Pro.jpg",
                            Holkeszult = "Capri",
                            Leiras = "Kilátás a hegyről",
                            Mikor = new DateTime(2018, 8, 6, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 10,
                            Filenev = "WP_20180806_12_05_57_Pro.jpg",
                            Holkeszult = "Capri",
                            Leiras = "Kilátás a hegyről, sziklák",
                            Mikor = new DateTime(2018, 8, 6, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 11,
                            Filenev = "WP_20180807_16_02_43_Pro.jpg",
                            Holkeszult = "Sorrento",
                            Leiras = "Kilátás az öbölre",
                            Mikor = new DateTime(2018, 8, 7, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 12,
                            Filenev = "WP_20180809_12_00_31_Pro.jpg",
                            Holkeszult = "Amalfi",
                            Leiras = "Tenger és hegyoldal",
                            Mikor = new DateTime(2018, 8, 9, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 13,
                            Filenev = "WP_20180809_14_00_53_Pro.jpg",
                            Holkeszult = "Amalfi",
                            Leiras = "Hegyoldal",
                            Mikor = new DateTime(2018, 8, 9, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 14,
                            Filenev = "WP_20180811_15_42_18_Pro.jpg",
                            Holkeszult = "Róma",
                            Leiras = "Angyalvár",
                            Mikor = new DateTime(2018, 8, 11, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        },
                        new
                        {
                            Id = 15,
                            Filenev = "WP_20180811_19_16_55_Pro.jpg",
                            Holkeszult = "Róma",
                            Leiras = "Colosseum",
                            Mikor = new DateTime(2018, 8, 11, 0, 0, 0, 0, DateTimeKind.Unspecified)
                        });
                });
#pragma warning restore 612, 618
        }
    }
}
