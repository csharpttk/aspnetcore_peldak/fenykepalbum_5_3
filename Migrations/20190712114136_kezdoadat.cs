﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace fenykepalbum_5_3.Migrations
{
    public partial class kezdoadat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Kepek",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Filenev = table.Column<string>(nullable: true),
                    Holkeszult = table.Column<string>(nullable: true),
                    Mikor = table.Column<DateTime>(nullable: false),
                    Leiras = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Kepek", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Kepek",
                columns: new[] { "Id", "Filenev", "Holkeszult", "Leiras", "Mikor" },
                values: new object[,]
                {
                    { 1, "WP_20150531_15_50_32_Pro.jpg", "Lednice", "Kastély park, tó", new DateTime(2015, 5, 31, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, "WP_20150531_16_01_58_Pro.jpg", "Lednice", "Kastély park, tó", new DateTime(2015, 5, 31, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, "WP_20161124_14_22_05_Pro.jpg", "ÓTátrafüred", "Felhőbe burkolózó hegycsúcs", new DateTime(2016, 11, 24, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 4, "WP_20171025_14_09_38_Pro.jpg", "Tátralomnic", "Hegycsúcs", new DateTime(2017, 10, 25, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 5, "WP_20171025_14_10_37_Pro.jpg", "Tátralomnic", "Kilátás a hegyről", new DateTime(2017, 10, 25, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 6, "WP_20171025_14_23_14_Pro.jpg", "Tátralomnic", "Lanovka", new DateTime(2017, 10, 25, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 7, "WP_20171025_14_35_07_Pro.jpg", "Tátralomnic", "Hegyi tó", new DateTime(2017, 10, 25, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 8, "WP_20180806_10_34_31_Pro.jpg", "Capri", "Kilátás a hegyről", new DateTime(2018, 8, 6, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 9, "WP_20180806_11_10_49_Pro.jpg", "Capri", "Kilátás a hegyről", new DateTime(2018, 8, 6, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 10, "WP_20180806_12_05_57_Pro.jpg", "Capri", "Kilátás a hegyről, sziklák", new DateTime(2018, 8, 6, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 11, "WP_20180807_16_02_43_Pro.jpg", "Sorrento", "Kilátás az öbölre", new DateTime(2018, 8, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 12, "WP_20180809_12_00_31_Pro.jpg", "Amalfi", "Tenger és hegyoldal", new DateTime(2018, 8, 9, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 13, "WP_20180809_14_00_53_Pro.jpg", "Amalfi", "Hegyoldal", new DateTime(2018, 8, 9, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 14, "WP_20180811_15_42_18_Pro.jpg", "Róma", "Angyalvár", new DateTime(2018, 8, 11, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 15, "WP_20180811_19_16_55_Pro.jpg", "Róma", "Colosseum", new DateTime(2018, 8, 11, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Kepek");
        }
    }
}
