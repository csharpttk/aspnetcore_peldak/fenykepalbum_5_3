﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace fenykepalbum_5_3.Models
{
    public class VáltakozóFényképek
    { 
        public (List<string>,List<string>) KépekHelye
        {
            get {
                Random r = new Random();
                List<string> képekútvonala = Directory.EnumerateFiles(@"wwwroot/img")
                                                .Select(x =>  x.Substring(7)).Take(3).ToList();  
                List<string> képekcíme= képekútvonala
                                                 .Select(x => x.Substring(3,x.Length-6)).Take(3).ToList();
                return (képekútvonala, képekcíme)   //tuple
                    ; }
               
        }        
    }
}
