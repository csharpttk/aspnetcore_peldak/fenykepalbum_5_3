﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using fenykepalbum_5_3.Models;

namespace fenykepalbum_5_3.Controllers
{
    public class HomeController : Controller
    {     
        public IActionResult Index()
        {
            VáltakozóFényképek váltakozó = new VáltakozóFényképek();
            (List<string> Képekútvonala, List<string> Képekcíme) = váltakozó.KépekHelye;
            ViewData["útvonalak"] = Képekútvonala;
            ViewData["címek"] = Képekcíme;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
